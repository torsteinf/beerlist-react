# Beerlist-react

Hold oversikten over øl du lagrer.

## Oversikt

En webapplikasjon laget med React-Redux. Googles [Firebase](https://firebase.google.com) sørger for database. SCSS og litt Bootstrap.

## Videre utvikling

* Legge til autentisering med Firebase slik at man kan opprette bruker og lage sin egen "ølkjeller".
* Få tilgang til Untappd sin API for automatisering av valg.
* Lage Android-app med [React Native](https://facebook.github.io/react-native/).

## Mappestruktur
![Oversikt over prosjektets mappestruktur](https://i.imgur.com/98pYi6z.png)
import React from 'react';
import { connect } from 'react-redux';
import { startGoogleLogin, startFacebookLogin } from '../actions/auth';

const LoginPage = ({ startGoogleLogin, startFacebookLogin }) => (
  <div className="login__background">
    <div className="box-layout">
      <div className="box-layout__box">
        <h1 className="box-layout__title">Beerlist</h1>
        <p>Hold orden på ølkjelleren din.</p>
        <button className="button" onClick={startGoogleLogin}>
          Logg inn med Google
        </button>
        <button className="button" onClick={startFacebookLogin}>
          Logg inn med Facebook
        </button>
        
        <p>En React-Redux-app.<br /> Se kildekode på Bitbucket:<br /> <a href="https://bitbucket.org/torsteinf/beerlist-react/">/torsteinf/beerlist-react</a>.</p>
      </div>
    </div>
  </div>
);

const mapDispatchToProps = (dispatch) => ({
  startGoogleLogin: () => dispatch(startGoogleLogin()),
  startFacebookLogin: () => dispatch(startFacebookLogin())
});
 
export default connect(undefined, mapDispatchToProps)(LoginPage);
import React from 'react';
import { connect } from 'react-redux';
import BeerListItem from './BeerListItem';
import selectBeers from '../selectors/beers';

const BeerList = (props) => (
  <div className="container"> 
    
    <table className="table-odd">
      <thead>
        <tr>
          <th className="th__name">Navn</th>
          <th className="th__brewery">Bryggeri</th>
          <th className="th__style">Stil</th>
          <th className="th__abv">ABV</th>
          <th className="th__ml">ML</th>
          <th className="th__amount">Antall</th>
          <th className="th__untappd">Untappd</th>
        </tr>
        </thead>
        <tbody>
      {props.beers.map((beer) => {
        return <BeerListItem key={beer.id} {...beer} />
      })}
      </tbody>
    </table>
  </div>
);

const mapStateToProps = (state) => {
  return {
    beers: selectBeers(state.beers, state.filters)
  };
};

export default connect(mapStateToProps)(BeerList);

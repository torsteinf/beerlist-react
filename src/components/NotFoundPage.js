import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
  <div>
    Denne siden eksisterer ikke. <Link to="/">Go tilbake til startsiden.</Link>
  </div>
);

export default NotFoundPage;
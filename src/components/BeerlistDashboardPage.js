import React from 'react';
import BeerList from './BeerList';
import BeerListFilters from './BeerListFilters';

const BeerlistDashboardPage = () => (
  <div>
    <BeerListFilters />
    <BeerList />
  </div>
);

export default BeerlistDashboardPage;
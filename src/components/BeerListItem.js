import React from 'react';
import{ Link } from 'react-router-dom';

const BeerListItem = ({ dispatch, id, brewery, name, style, abv, ml, untappdid, untappdscore, amount }) => (
  <tr>
    <td className="td__name"><Link to={`/edit/${id}`}>{name}</Link></td> 
    <td className="td__brewery">{brewery}</td> 
    <td className="td__style">{style}</td>  
    <td className="td__abv">{abv}</td> 
    <td className="td__ml">{ml}</td> 
    <td className="td__amount">{amount}</td> 
    <td className="td__untappd"><a href={`https://untappd.com/beer/${untappdid}`}>{untappdscore}</a></td> 
  </tr>
);

export default BeerListItem;
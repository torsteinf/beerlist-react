import React from 'react';

const Loadingpage = () => (
  <div className="loader">
    <div className="loading-spinner"></div>
  </div>
);

export default Loadingpage;
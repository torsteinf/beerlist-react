import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startLogout } from '../actions/auth';
import ReactGA from 'react-ga';

function headerHandleClick() {
  ReactGA.event({
    category: 'Header',
    action: 'Clicked dashboard link'
  })
}

const Header = ({startLogout}) => (
  <header className="header">
    <div className="container">
      <div className="header__content">
        <div>
          <Link to="/dashboard" className="header__title" onClick={headerHandleClick}>
            Ølkjelleren
          </Link>
        </div>
        <div>
          <Link to="/add">
            <button className="button">
            Legg til øl 
            </button>
          </Link>
            

          <button className="button" onClick={startLogout}>
            Logg ut
          </button>
        </div>

      </div>
    </div>
  </header>
);

const mapDispatchToProps = (dispatch) => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(undefined, mapDispatchToProps)(Header);
import React from 'react';
import { connect } from 'react-redux';
import { setTextFilter, sortByAbv, sortByAmount, sortByBrewery, sortByName, sortByStyle, sortByUntappd } from '../actions/filters';

export class BeerListFilters extends React.Component {
  onTextChange = (e) => {
    this.props.setTextFilter(e.target.value);
  };
  onSortChange = (e) => {
    if (e.target.value === 'abv') {
      this.props.sortByAbv();
    } else if (e.target.value === 'amount') {
      this.props.sortByAmount();
    } else if (e.target.value === 'brewery') {
      this.props.sortByBrewery();
    } else if (e.target.value === 'name') {
      this.props.sortByName();
    } else if (e.target.value === 'style') {
      this.props.sortByStyle();
    } else if (e.target.value === 'untappd') {
      this.props.sortByUntappd();
    }
  };

  render() {
    return (
      <div className="container">
        <div className="input-group">
          <div className="input-group__item">
            <input
              type="text"
              className="text-input"
              placeholder="Søk på navn"
              value={this.props.filters.text}
              onChange={this.onTextChange}
            />
          </div>
          <div className="input-group__item">
            <select
              className="select"
              value={this.props.filters.sortBy}
              onChange={this.onSortChange}
            >
              <option value="brewery">Bryggeri</option>
              <option value="name">Navn</option>
              <option value="style">Stil</option>
            </select>
          </div>
        </div>
      </div>
    )
  }
};


const mapStateToProps = (state) => ({
    filters: state.filters
});

const mapDispatchToProps = (dispatch) => ({
  setTextFilter: (text) => dispatch(setTextFilter(text)),
  sortByAbv: () => dispatch(sortByAbv()),
  sortByAmount: () => dispatch(sortByAmount()),
  sortByBrewery: () => dispatch(sortByBrewery()),
  sortByName: () => dispatch(sortByName()),
  sortByStyle: () => dispatch(sortByStyle()),
  sortByUntappd: () => dispatch(sortByUntappd())
});

export default connect(mapStateToProps, mapDispatchToProps)(BeerListFilters);
import React from 'react';
import { connect } from 'react-redux';
import BeerForm from './BeerForm';
import { startEditBeer, startRemoveBeer } from '../actions/beers';

export class EditBeerPage extends React.Component {
  onSubmit = (beer) => {
    this.props.startEditBeer(this.props.beer.id, beer);
    this.props.history.push('/');
  };
  onRemove = () => {
    this.props.startRemoveBeer({ id: this.props.beer.id });
    this.props.history.push('/');
  };
  render() {
    return (
      <div className="container">
        <BeerForm
          beer={this.props.beer}
          onSubmit={this.onSubmit}
        />
        <button onClick={this.onRemove} className="button">Fjern øl</button>
      </div>
    );
  }
};

const mapStateToProps = (state, props) => {
  return {
    beer: state.beers.find((beer) => beer.id === props.match.params.id)
  };
};

const mapDispatchToProps = (dispatch, props) => ({
  startEditBeer: (id, beer) => dispatch(startEditBeer(id, beer)),
  startRemoveBeer: (data) => dispatch(startRemoveBeer(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditBeerPage);
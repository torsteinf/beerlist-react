import React from 'react';

export default class BeerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      brewery: props.beer ? props.beer.brewery : '',
      name: props.beer ? props.beer.name : '',
      style: props.beer ? props.beer.style : '',
      abv: props.beer ? props.beer.abv : '',
      amount: props.beer ? props.beer.amount : '',
      ml: props.beer ? props.beer.ml : '',
      untappdid: props.beer ? props.beer.untappdid : '',
      untappdscore: props.beer ? props.beer.untappdscore : '',
    };
  }
  onNameChange = (e) => {
    const name = e.target.value;
    this.setState(() => ({ name }));
  };
  onBreweryChange  = (e) => {
    const brewery = e.target.value;
    this.setState(() => ({ brewery }));
  };
  onStyleChange = (e) => {
    const style = e.target.value;
    this.setState(() => ({ style }));
  };
  onAbvChange = (e) => {
    const abv = e.target.value;
    if (abv.match(/^\d*(\.\d{0,2})?$/)) {
      this.setState(() => ({ abv }));
    }
  };
  onAmountChange = (e) => {
    const amount = e.target.value;
    if (amount.match(/^\d+$/)) {
      this.setState(() => ({ amount }));
    }
  };
  onMlChange = (e) => {
    const ml = e.target.value;
    if (ml.match(/^\d+$/)) {
      this.setState(() => ({ ml }));
    }
  };
  onUntappdidChange = (e) => {
    const untappdid = e.target.value;
    if (untappdid.match(/^\d*(\.\d{0,2})?$/)) {
      this.setState(() => ({ untappdid }));
    }
  };
  onUntappdScoreChange = (e) => {
    const untappdscore = e.target.value;
    if (untappdscore.match(/^\d*(\.\d{0,2})?$/)) {
      this.setState(() => ({ untappdscore }));
    }
  };

  onSubmit = (e) => {
    e.preventDefault();

    if(!this.state.name) {
      this.setState(() => ({ error: 'Vennligst legg til navn'}));
    } else {
      this.setState(() => ({ error: ''}));
      this.props.onSubmit({
        brewery: this.state.brewery,
        name: this.state.name,
        abv: this.state.abv ,
        style: this.state.style ,
        ml: this.state.ml ,
        amount: this.state.amount ,
        untappdid: this.state.untappdid ,
        untappdscore: this.state.untappdscore
      })
    }
  }  
  
  render() {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.onSubmit} className="form">
              <input
                type="text"
                id="inputName"
                className="text-input"
                placeholder="Navn"
                value={this.state.name}
                onChange={this.onNameChange}
              />
              <input
                type="text"
                className="text-input"
                placeholder="Bryggeri"
                value={this.state.brewery}
                onChange={this.onBreweryChange}
              />
              <input
                type="number"
                className="text-input"
                placeholder="ABV"
                value={this.state.abv}
                onChange={this.onAbvChange}
              />
              <input
                type="text"
                className="text-input"
                placeholder="Øltype"
                value={this.state.style}
                onChange={this.onStyleChange}
              />
              <input
                type="number"
                className="text-input"
                placeholder="Antall flasker"
                value={this.state.amount}
                onChange={this.onAmountChange}
              />
              <input
                type="number"
                className="text-input"
                placeholder="Milliliter"
                value={this.state.ml}
                onChange={this.onMlChange}
              />
              <input
                type="number"
                className="text-input"
                placeholder="Untappd-ID"
                value={this.state.untappdid}
                onChange={this.onUntappdidChange}
              />
              <input
                type="number"
                className="text-input"
                placeholder="Untappd-rating"
                value={this.state.untappdscore}
                onChange={this.onUntappdScoreChange}
              />
              <input 
                type="submit" 
                id="submit" 
                className="button"
                value="Lagre øl"
              />
        </form>
      </div>
    )
  }
}
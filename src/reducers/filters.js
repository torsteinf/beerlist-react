const filtersReducerDefaultState = {
  text: '',
  sortBy: 'name'
};

export default (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case 'SET_TEXT_FILTER':
      return {
        ...state,
        text: action.text
      };
    case 'SORT_BY_ABV':
      return {
        ...state,
        sortBy: 'abv'
      }
      case 'SORT_BY_AMOUNT':
      return {
        ...state,
        sortBy: 'amount'
      }
      case 'SORT_BY_BREWERY':
      return {
        ...state,
        sortBy: 'brewery'
      }
      case 'SORT_BY_ML':
      return {
        ...state,
        sortBy: 'ml'
      }
      case 'SORT_BY_NAME':
      return {
        ...state,
        sortBy: 'name'
      }
      case 'SORT_BY_STYLE':
      return {
        ...state,
        sortBy: 'style'
      }
      case 'SORT_BY_UNTAPPD':
      return {
        ...state,
        sortBy: 'untappd'
      }
    default:
      return state;
  }
}
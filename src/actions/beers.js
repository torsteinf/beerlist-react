import uuid from 'uuid';
import database from '../firebase/firebase';

// Legg til øl ADD_BEER
export const addBeer = (beer) => ({
  type: 'ADD_BEER',
  beer
});

export const startAddBeer = (beerData = {}) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;
    const {
      brewery = '',
      name = '',
      style = '',
      abv = 0,
      ml = 0,
      amount = 0,
      untappdid = 0,
      untappdscore = 0
    } = beerData;
    const beer = { brewery, name, style, abv, ml, amount, untappdid, untappdscore };

    database.ref(`users/${uid}/beers`).push(beer).then((ref) => {
      dispatch(addBeer({
        id: ref.key,
        ...beer
      }));
    });
  };
};

// Slett øl REMOVE_BEER
export const removeBeer = ({ id } = {}) => ({
  type: 'REMOVE_BEER',
  id
});

export const startRemoveBeer = ({ id }) => {
  return(dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`users/${uid}/beers/${id}`).remove().then(() => {
      dispatch(removeBeer({id}));
    });
  };
};

// Rediger øl EDIT_BEER
export const editBeer = (id, updates) => ({
  type: 'EDIT_BEER',
  id,
  updates
});

export const startEditBeer = (id, updates) => {
  return(dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`users/${uid}/beers/${id}`).update(updates).then(() => {
      dispatch(editBeer(id, updates));
    });
  };
};

// SET_BEERS
export const setBeers = (beers) => ({
  type: 'SET_BEERS',
  beers
});

export const startSetBeers = () => {
  return(dispatch, getState) => {
    const uid = getState().auth.uid;
    return database.ref(`users/${uid}/beers`)
    .once('value')
    .then((snapshot) => {
      const beers = [];

      snapshot.forEach((childSnapshot) => {
        beers.push({
          id: childSnapshot.key,
          ...childSnapshot.val()
        });
      });

      dispatch(setBeers(beers));
    });
  };
};
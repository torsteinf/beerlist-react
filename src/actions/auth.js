import React from 'react';
import { firebase, googleAuthProvider, facebookAuthProvider } from '../firebase/firebase';

export const login = (uid) => ({
  type: 'LOGIN',
  uid
});

export const startGoogleLogin = () => {
  return () => {
    return firebase.auth().signInWithPopup(googleAuthProvider);
  };
};

export const startFacebookLogin = () => {
  return () => {
     return firebase.auth().signInWithPopup(facebookAuthProvider)
     .catch(function(error) {
      if (error.code === 'auth/account-exists-with-different-credential') {
        console.log('Konto eksisterer allerede med denne epostadressen');
      }
      
       const errorCode = error.code;
       const errorMessage = error.message;
       const credential = error.credential;
     });
    
  };
};

export const logout = () => ({
  type: 'LOGOUT'
});

export const startLogout = () => {
  return () => {
    return firebase.auth().signOut();
  };
};
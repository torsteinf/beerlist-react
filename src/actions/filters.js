//SET_TEXT
export const setTextFilter = (text = '') => ({
  type: 'SET_TEXT_FILTER',
  text
})

//SORT_BY_ABV
export const sortByAbv = () => ({
  type: 'SORT_BY_ABV'
}); 

//SORT_BY_AMOUNT
export const sortByAmount = () => ({
  type: 'SORT_BY_AMOUNT'
});

//SORT_BY_BREWERY
export const sortByBrewery = () => ({
  type: 'SORT_BY_BREWERY'
});

//SORT_BY_ML
export const sortByMl = () => ({
  type: 'SORT_BY_ML'
});

//SORT_BY_NAME
export const sortByName = () => ({
  type: 'SORT_BY_NAME'
});

//SORT_BY_STYLE
export const sortByStyle = () => ({
  type: 'SORT_BY_STYLE'
});

//SORT_BY_UNTAPPD
export const sortByUntappd = () => ({
  type: 'SORT_BY_UNTAPPD'
});
import React from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import AddBeerPage from '../components/AddBeerPage';
import EditBeerPage from '../components/EditBeerPage';
import BeerlistDashboardPage from '../components/BeerlistDashboardPage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import ReactGA from 'react-ga';


ReactGA.initialize('UA-113515769-1');

function gaTracking() {
  ReactGA.pageview(window.location.pathname + window.location.search);
}


export const history = createHistory();

const AppRouter = () => (
  <Router history={history} onUpdate={gaTracking}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={ true } />
        <PrivateRoute path="/dashboard" component={BeerlistDashboardPage} exact={ true } />
        <PrivateRoute path="/add" component={AddBeerPage} />
        <PrivateRoute path="/edit/:id" component={EditBeerPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
export default (beers, { text, sortBy }) => {
  return beers.filter((beer) => {
    const textMatch = beer.name.toLowerCase().includes(text.toLowerCase());
    return textMatch;
  }).sort((a, b) => {
    if (sortBy === 'abv') {
      if (a.abv < b.abv) return -1;
      if (a.abv > b.abv) return 1;
      return 0;
    } else if (sortBy === 'amount') {
      return a.amount < b.amount ? 1 : -1;
    } else if (sortBy === 'brewery') {
      if (a.brewery < b.brewery) return -1;
      if (a.brewery > b.brewery) return 1;
      return 0;
    } else if (sortBy === 'name') {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    } else if (sortBy === 'style') {
      if (a.style < b.style) return -1;
      if (a.style > b.style) return 1;
      return 0;
    }
    //HER MÅ RESTEN SOM SKAL SORTERES LEGGES INN
  });
};